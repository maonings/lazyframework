# LazyFramework

###  **Java开发基础框架，仅供个人工作和学习使用，持续更新** ...

#### 模块介绍
- commons

  基础工具包，除了归纳常用的工具类外，主要还对RESTfull API接口返回数据模型和异常状态码进行了统一管理。

- security-admin
  
  基于SpringSecurity的简单后台安全框架，实现JWT认证。
  引入此安全框架只需添加一个注解（@EnableLazySecurity）和实现两个接口（RbacService、UserDetailsService）
  
- security-web
  
  仿照SpringSecurity源码实现的一个及其简单但功能能满足基本需求的用户端接口权限控制(认证、授权模型)，支持用户名密码、微信OpenID登录方式，使用JWT作为认证凭证，通过配置文件可实现简单的角色权限控制。 

  
### 依赖引入
1. 在pom.xml文件中添加仓库[Nexus私服地址](http://maven.maonings.com/repository/maven-public/)，这是我在阿里云上自己用nexus搭的私服仓库。
这里需要注意一点如果你的maven配置文件(setting.xml)中更换了中央仓库，一定记得不要把mirrorOf的值设为'*'!!!
```xml
<repositories>
    <repository>
        <id>lazy-repo</id>
        <url>http://maven.maonings.com/repository/maven-public/</url>
        <snapshots>
            <enabled>true</enabled>
            <updatePolicy>always</updatePolicy>
        </snapshots>
    </repository>
</repositories>
```

2、添加依赖

```xml
<dependency>
    <groupId>com.maonings</groupId>
    <artifactId>lazyframework-commons</artifactId>
    <version>1.0.0-SNAPSHOT</version>
</dependency>
<dependency>
    <groupId>com.maonings.security</groupId>
    <artifactId>lazyframework-security-admin</artifactId>
    <version>1.0.0-SNAPSHOT</version>
</dependency>
<dependency>
    <groupId>com.maonings.security</groupId>
    <artifactId>lazyframework-security-web</artifactId>
    <version>1.0.0-SNAPSHOT</version>
</dependency>
...
```

