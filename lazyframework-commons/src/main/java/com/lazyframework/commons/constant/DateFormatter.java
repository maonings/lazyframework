package com.lazyframework.commons.constant;

/**
 * 常用到的日期时间格式化Pattern
 */
public interface DateFormatter {

    String PATTERN_01 = "yyyy-MM-dd";
    String PATTERN_02 = "yyyy/MM/dd";
    String PATTERN_03 = "yyyyMMdd";


    String PATTERN_04 = "yyyy-MM";
    String PATTERN_05 = "yyyy/MM";
    String PATTERN_06 = "yyyyMM";

    String PATTERN_07 = "yy-MM";
    String PATTERN_08 = "yy/MM";
    String PATTERN_09 = "yyMM";

    String PATTERN_10 = "yyyy-MM-dd HH:mm:ss";
    String PATTERN_11 = "yyyy/MM/dd HH:mm:ss";
    String PATTERN_12 = "yyyyMMddHHmmss";

    String PATTERN_13 = "yy-MM-dd HH:mm:ss";
    String PATTERN_14 = "yy/MM/dd HH:mm:ss";
    String PATTERN_15 = "yyMMddHHmmss";

}
