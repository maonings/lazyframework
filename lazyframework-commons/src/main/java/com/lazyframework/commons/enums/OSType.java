package com.lazyframework.commons.enums;

/**
 * 操作系统枚举
 */
public enum OSType {

    LINUX, WINDOWS;

    OSType() {

    }

    public static boolean isLinux() {
        return LINUX.toString().equals(getOs());
    }

    public static boolean isWin() {
        return getOs().startsWith("WIN");
    }

    private static String getOs() {
        return System.getProperty("os.name").toUpperCase();
    }

}
