package com.lazyframework.commons.jackson;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.lazyframework.commons.util.RMBUtils;

import java.io.IOException;

/**
 * 人民币字段值序列化（单位由分转换为元）
 *
 * @author lazy
 */
public class Fen2YuanSerializer extends JsonSerializer<String> {

    @Override
    public void serialize(String value, JsonGenerator gen, SerializerProvider serializers)
            throws IOException {
        gen.writeString(StringUtils.isNotEmpty(value) ? RMBUtils.fen2Yuan(value) : "");
    }

}
