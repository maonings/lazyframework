package com.lazyframework.commons.jackson;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.lazyframework.commons.util.RMBUtils;

import java.io.IOException;

/**
 * 分民币字段反序列化器（元转int类型的分）
 *
 * @author lazy
 */
public class Yuan2FenDeserializer extends JsonDeserializer<String> {

    @Override
    public String deserialize(JsonParser p, DeserializationContext ctxt)
            throws IOException {
        String value = p.getValueAsString();
        return StringUtils.isNotEmpty(value) ? String.valueOf(RMBUtils.yuan2Fen(value)) : null;
    }

}
