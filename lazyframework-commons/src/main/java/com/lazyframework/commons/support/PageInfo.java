package com.lazyframework.commons.support;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Collection;

/**
 * Create by lazy in 2019/9/29
 */
@Data
@ApiModel("分页对象")
public class PageInfo<T> {

    @ApiModelProperty(value = "页码", position = 1)
    private int page;

    @ApiModelProperty(value = "每页条数", position = 2)
    private int size;

    @ApiModelProperty(value = "总页数", position = 3)
    private int totalPage;

    @ApiModelProperty(value = "总记录数", position = 4)
    private long totalElements;

    @ApiModelProperty(value = "查询内容", position = 5)
    private Collection<T> content;

}
