package com.lazyframework.commons.support;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Create by lazy in 2019/9/28
 */
@Data
@ApiModel("分页参数")
public class Pageable {

    @ApiModelProperty(value = "页码", position = 1, example = "1")
    private int page = 1;

    @ApiModelProperty(value = "每页条数", position = 2, example = "15")
    private int size = 10;

    @ApiModelProperty(value = "排序字段", position = 3, example = "id asc", notes = "复合排序用逗号分割，如id asc, username desc, ...")
    private String sort;

}
