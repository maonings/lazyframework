package com.lazyframework.commons.support;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 定义全局请求对象模型
 * Create by lazy in 2019/9/26
 */
@Data
@ApiModel
public class ResponseModel<T> implements Serializable {

    @ApiModelProperty(value = "状态码", position = 1)
    private int code;

    @ApiModelProperty(value = "提示信息", position = 2)
    private String msg;

    @ApiModelProperty(value = "请求数据", position = 3)
    private T data;

    @ApiModelProperty(value = "响应时间戳", position = 4)
    private long timestamp;

    public ResponseModel(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.timestamp = System.currentTimeMillis();
    }

    public ResponseModel(ResponseStatus status) {
        this(status, null);
    }

    public ResponseModel(ResponseStatus status, T data) {
        this(status.value(), status.reasonPhrase(), data);
    }

}
