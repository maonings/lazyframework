package com.lazyframework.commons.support;

/**
 * Create by lazy in 2019.09.17
 */
public class Snowflake {

    /**
     * 起始时间： 2019-09-20 18:00:00
     */
    private static final long BASIC_TIMESTAMP = 1568973600000L;

    private static final int MAX_WORKER_ID = 31;

    private static final int MAX_DATA_CENTER_ID = 31;

    /**
     * 工作机器ID 0-31
     */
    private int workerId;

    /**
     * 数据中心ID 0-31
     */
    private int dataCenterId;

    private int sequence = 0;

    private long lastTimestamp;

    public Snowflake(int workerId, int dataCenterID) {
        if (workerId < 0 || workerId > MAX_WORKER_ID) {
            throw new IllegalArgumentException("The workerId must between 0 and 31");
        }
        if (dataCenterID < 0 || dataCenterID > MAX_DATA_CENTER_ID) {
            throw new IllegalArgumentException("The dataCenterId must between 0 and 31");
        }
        this.workerId = workerId;
        this.dataCenterId = dataCenterID;
    }

    // ==============================Methods==========================================

    /**
     * 返回String类型
     * @return
     */
    public String nextAsString() {
        return String.valueOf(next());
    }

    /**
     * 获得下一个ID (该方法是线程安全的)
     * @return SnowflakeId
     */
    public synchronized long next() {
        long timestamp = timeGen();

        //如果当前时间小于上一次ID生成的时间戳，说明系统时钟回退过这个时候应当抛出异常
        if (timestamp < lastTimestamp) {
            throw new RuntimeException(
                    String.format("Clock moved backwards. Refusing to generate id for %d milliseconds", lastTimestamp - timestamp));
        }

        //如果是同一时间生成的，则进行毫秒内序列
        if (lastTimestamp == timestamp) {
            int sequenceMask = 4095;
            sequence = (sequence + 1) & sequenceMask;
            //毫秒内序列溢出
            if (sequence == 0) {
                //阻塞到下一个毫秒,获得新的时间戳
                timestamp = tilNextMillis(lastTimestamp);
            }
        }
        //时间戳改变，毫秒内序列重置
        else {
            sequence = 0;
        }

        //上次生成ID的时间截
        lastTimestamp = timestamp;

        //移位并通过或运算拼到一起组成64位的ID
        return ((timestamp - BASIC_TIMESTAMP) << 22) | (workerId << 17) | (dataCenterId << 12) | sequence;
    }

    /**
     * 阻塞到下一个毫秒，直到获得新的时间戳
     * @param lastTimestamp 上次生成ID的时间截
     * @return 当前时间戳
     */
    protected long tilNextMillis(long lastTimestamp) {
        long timestamp = timeGen();
        while (timestamp <= lastTimestamp) {
            timestamp = timeGen();
        }
        return timestamp;
    }

    /**
     * 返回以毫秒为单位的当前时间
     * @return 当前时间(毫秒)
     */
    protected long timeGen() {
        return System.currentTimeMillis();
    }

}
