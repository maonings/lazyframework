package com.lazyframework.commons.support.exception;

import com.lazyframework.commons.support.ResponseStatus;

/**
 * 封装业务异常
 *
 * Create by lazy in 2019/10/11
 */
public class BusinessException extends RuntimeException {

    private ResponseStatus rs;

    /**
     * 传入异常状态码
     * @param rs
     */
    public BusinessException(ResponseStatus rs) {
        super(rs.reasonPhrase());
        this.rs = rs;
    }

    /**
     * 返回业务异常状态码
     * @return ResponseStatus
     */
    public ResponseStatus getRs() {
        return rs;
    }
}
