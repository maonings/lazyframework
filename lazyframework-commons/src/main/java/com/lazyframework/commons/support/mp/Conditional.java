package com.lazyframework.commons.support.mp;

import java.lang.annotation.*;

/**
 * Create by lazy in 2019/10/25
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = ElementType.FIELD)
@Documented
public @interface Conditional {

    String column() default "";

    boolean ignore() default false;

    ConditionalType value() default ConditionalType.DEFAULT;

}
