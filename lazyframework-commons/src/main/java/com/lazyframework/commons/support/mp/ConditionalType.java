package com.lazyframework.commons.support.mp;

/**
 * Create by lazy in 2019/10/25
 */
public enum ConditionalType {

    DEFAULT, EQ, LIKE, IN, LE, GE, LT, GT, IS_NULL, NOT_NULL, NE, NOT_IN

}
