package com.lazyframework.commons.support.mp;

/**
 * Create by lazy in 2019/10/8
 */
public interface DbEnum {

    int value();

    String label();

}
