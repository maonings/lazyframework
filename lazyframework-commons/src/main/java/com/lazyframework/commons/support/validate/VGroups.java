package com.lazyframework.commons.support.validate;

import javax.validation.groups.Default;

public interface VGroups {

    interface Add extends Default {}

    interface Edit extends Default {}

}
