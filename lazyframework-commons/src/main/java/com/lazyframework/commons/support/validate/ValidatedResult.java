package com.lazyframework.commons.support.validate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Create by lazy in 2019/10/15
 */
@Data
@ApiModel("参数校验返回值")
public class ValidatedResult implements Serializable {

    @ApiModelProperty(value = "字段名", position = 1)
    private String field;

    @ApiModelProperty(value = "错误信息", position = 2)
    private String message;

}
