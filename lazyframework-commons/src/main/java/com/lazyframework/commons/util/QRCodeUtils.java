package com.lazyframework.commons.util;

import com.google.zxing.*;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Hashtable;

/**
 * 二维码工具类
 *
 * 借鉴自CSDN <a href="https://blog.csdn.net/jam_fanatic/article/details/82818857">查看原文</a>
 */
public class QRCodeUtils {

    private static final String CHARSET = "utf-8";

    // LOGO宽度
    private static final int LOGO_SIZE = 80;

    // 二维码尺寸
    private static final int QR_CODE_SIZE = 300;

    private static final String OUTPUT_IMAGE_FORMAT = "PNG";

    /**
     * 生成并且保存二维码到指定位置
     * @param content
     * @param outputPath
     * @return
     * @throws IOException
     * @throws WriterException
     */
    public static String encodeAndSave(String content, String outputPath)
            throws IOException, WriterException {
        return encodeAndSave(content, null, outputPath);
    }

    /**
     * 生成并且保存二维码到指定位置
     * @param content
     * @param logo
     * @param outputPath
     * @return
     * @throws IOException
     * @throws WriterException
     */
    public static String encodeAndSave(String content, InputStream logo, String outputPath)
            throws IOException, WriterException {
        return encodeAndSave(content, logo, true, outputPath);
    }

    /**
     * 生成并且保存二维码到指定位置
     * @param content
     * @param logo
     * @param needCompress
     * @param outputPath
     * @return
     * @throws IOException
     * @throws WriterException
     */
    public static String encodeAndSave(String content, InputStream logo, boolean needCompress, String outputPath)
            throws IOException, WriterException {
        BufferedImage image = encode(content, logo, needCompress);

        File output = new File(outputPath);
        output.mkdirs();

        String outputFile = getOutputFilename(outputPath);

        ImageIO.write(image, OUTPUT_IMAGE_FORMAT, new FileOutputStream(outputFile));

        return outputFile;
    }

    private static String getOutputFilename(String outputPath) {
        return outputPath + "/" + System.currentTimeMillis() + "." + OUTPUT_IMAGE_FORMAT.toLowerCase();
    }

    /**
     * 生成二维码不带logo
     * @param content
     * @return
     * @throws IOException
     * @throws WriterException
     */
    public static BufferedImage encode(String content)
            throws IOException, WriterException {
        return encode(content, null, false);
    }

    /**
     * 生成二维码 并且在中间位置插入Logo
     *
     * @param content 内容
     * @param logoInputStream logo输入流
     * @return BufferedImage
     * @throws IOException
     * @throws WriterException
     */
    public static BufferedImage encode(String content, InputStream logoInputStream)
            throws IOException, WriterException {
        return encode(content, logoInputStream, true);
    }

    /**
     * 生成二维码 并且在中间位置插入Logo
     * @param content 内容
     * @param logoInputStream logo输入流
     * @param needCompress 是否压缩logo到固定大小
     * @return
     * @throws IOException
     * @throws WriterException
     */
    public static BufferedImage encode(String content, InputStream logoInputStream, boolean needCompress)
            throws IOException, WriterException {
        return QRCodeUtils.createImage(content, logoInputStream, needCompress);
    }

    /**
     * 解码
     * @param inputStream
     * @return
     * @throws Exception
     */
    public static String decode(InputStream inputStream) throws Exception {
        BufferedImage image = ImageIO.read(inputStream);
        if (image == null) {
            return null;
        }
        BufferedImageLuminanceSource source = new BufferedImageLuminanceSource(image);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        Hashtable<DecodeHintType, Object> hints = new Hashtable<>();
        hints.put(DecodeHintType.CHARACTER_SET, CHARSET);
        Result result = new MultiFormatReader().decode(bitmap, hints);
        return result.getText();
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~ private methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    /**
     * 插入Logo
     * @param source
     * @param logoInputStream
     * @param needCompress
     * @throws IOException
     */
    private static void insertImage(BufferedImage source, InputStream logoInputStream, boolean needCompress) throws IOException {

        Image src = ImageIO.read(logoInputStream);

        int width = src.getWidth(null);
        int height = src.getHeight(null);
        if (needCompress) { // 压缩LOGO
            height = width = LOGO_SIZE;
            Image image = src.getScaledInstance(width, height, Image.SCALE_SMOOTH);
            BufferedImage tag = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics g = tag.getGraphics();
            g.drawImage(image, 0, 0, null); // 绘制缩小后的图
            g.dispose();
            src = image;
        }

        // 插入LOGO
        Graphics2D graph = source.createGraphics();
        int x = (QR_CODE_SIZE - width) / 2;
        int y = (QR_CODE_SIZE - height) / 2;
        graph.drawImage(src, x, y, width, height, null);
        Shape shape = new RoundRectangle2D.Float(x, y, width, width, 6, 6);
        graph.setStroke(new BasicStroke(3f));
        graph.draw(shape);
        graph.dispose();
    }

    /**
     * 生成二维码
     * @param content
     * @param logoInputStream
     * @param needCompress
     * @return
     * @throws IOException
     * @throws WriterException
     */
    private static BufferedImage createImage(String content, InputStream logoInputStream, boolean needCompress)
            throws IOException, WriterException {
        Hashtable<EncodeHintType, Object> hints = new Hashtable<>();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        hints.put(EncodeHintType.CHARACTER_SET, CHARSET);
        hints.put(EncodeHintType.MARGIN, 1);

        BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, QR_CODE_SIZE,
                QR_CODE_SIZE, hints);

        int width = bitMatrix.getWidth();
        int height = bitMatrix.getHeight();
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                image.setRGB(x, y, bitMatrix.get(x, y) ? 0xFF000000 : 0xFFFFFFFF);
            }
        }

        if (logoInputStream == null) {
            return image;
        }

        // 插入图片
        QRCodeUtils.insertImage(image, logoInputStream, needCompress);

        return image;
    }

}
