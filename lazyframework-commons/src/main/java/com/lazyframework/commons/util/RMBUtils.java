package com.lazyframework.commons.util;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Create by lazy in 2019/11/1
 */
public class RMBUtils {

    private static final BigDecimal RATE_FEN_TO_YUAN = new BigDecimal(100);

    private RMBUtils() {}

    /**
     * 分转元，保留小数点后两位
     * @param value
     * @return
     */
    public static String fen2Yuan(int value) {
        return fen2Yuan(value, 2);
    }

    /**
     * 分转元，保留指定小数点位数
     * @param value
     * @param scale
     * @return
     */
    public static String fen2Yuan(int value, int scale) {
        return fen2Yuan(String.valueOf(value), scale);
    }

    /**
     * 分转元，保留小数点后两位
     * @param value
     * @return
     */
    public static String fen2Yuan(long value) {
        return fen2Yuan(value, 2);
    }

    /**
     * 分转元，保留小数点后两位
     * @param value
     * @return
     */
    public static String fen2Yuan(long value, int scale) {
        return fen2Yuan(String.valueOf(value), scale);
    }

    /**
     * 分转元，保留小数点后两位
     * @param value
     * @return
     */
    public static String fen2Yuan(String value) {
        return fen2Yuan(value, 2);
    }

    /**
     * 分转元，保留指定小数点位数
     * @param value
     * @param scale
     * @return
     */
    public static String fen2Yuan(String value, int scale) {
        if (StringUtils.isEmpty(value)) {
            return "0.00";
        }
        BigDecimal fen = new BigDecimal(value);
        BigDecimal yuan = fen.divide(RATE_FEN_TO_YUAN, scale, RoundingMode.HALF_UP);
        return yuan.toString();
    }

    /**
     * 元转分
     * <h1>Example</h1>
     * <table>
     *     <tr>
     *         <td align="right">125.63</td> <td width="30px"></td> <td>12563</td>
     *     </tr>
     *     <tr>
     *         <td align="right">125.632</td> <td width="30px"></td> <td>12563</td>
     *     </tr>
     * </table>
     * @param value
     * @return
     */
    public static int yuan2Fen(int value) {
        return yuan2Fen(String.valueOf(value));
    }

    /**
     * 元转分
     * <h1>Example</h1>
     * <table>
     *     <tr>
     *         <td align="right">125.63F</td> <td width="30px"></td> <td>12563</td>
     *     </tr>
     *     <tr>
     *         <td align="right">125.632F</td> <td width="30px"></td> <td>12563</td>
     *     </tr>
     * </table>
     * @param value
     * @return
     */
    public static int yuan2Fen(float value) {
        return yuan2Fen(String.valueOf(value));
    }

    /**
     * 元转分
     * <h1>Example</h1>
     * <table>
     *     <tr>
     *         <td align="right">125.63</td> <td width="30px"></td> <td>12563</td>
     *     </tr>
     *     <tr>
     *         <td align="right">125.632</td> <td width="30px"></td> <td>12563</td>
     *     </tr>
     * </table>
     * @param value
     * @return
     */
    public static int yuan2Fen(double value) {
        return yuan2Fen(String.valueOf(value));
    }

    /**
     * 元转分
     * <h1>Example</h1>
     * <table>
     *     <tr>
     *         <td align="right">"125.63"</td> <td width="30px"></td> <td>12563</td>
     *     </tr>
     *     <tr>
     *         <td align="right">"125.632"</td> <td width="30px"></td> <td>12563</td>
     *     </tr>
     * </table>
     * @param value
     * @return
     */
    public static int yuan2Fen(String value) {
        if (StringUtils.isEmpty(value)) {
            return 0;
        }
        BigDecimal yuan = new BigDecimal(value);
        BigDecimal fen = yuan.multiply(RATE_FEN_TO_YUAN);
        return fen.intValue();
    }

    /**
     * 返回Long类型数字
     * @param value
     * @return
     */
    public static long yuan2FenAsLong(String value) {
        if (StringUtils.isEmpty(value)) {
            return 0L;
        }
        BigDecimal yuan = new BigDecimal(value);
        BigDecimal fen = yuan.multiply(RATE_FEN_TO_YUAN);
        return fen.longValue();
    }

}
