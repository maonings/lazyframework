package com.lazyframework.security.admin.access;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;

/**
 * Create by lazy in 2019.09.17
 */
@Slf4j
public class DefaultRbacServiceImpl implements RbacService {

    @Override
    public boolean hasPermission(HttpServletRequest request, Authentication authentication) {
        log.warn("RbacServiceImpl is missing, this is default. [request uri: {}]", request.getRequestURI());
        return true;
    }

}
