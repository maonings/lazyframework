package com.lazyframework.security.admin.access;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;

/**
 * 基于资源的访问控制
 */
public interface RbacService {

    boolean hasPermission(HttpServletRequest request, Authentication authentication);

}
