package com.lazyframework.security.admin.annotation;

import com.lazyframework.security.admin.configuration.AutowiredBeanConfigurer;
import com.lazyframework.security.admin.configuration.CompositeSecurityConfigurer;
import com.lazyframework.security.admin.properties.SecurityProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Import custom security configurer.
 * <p>
 * Create by lazy in 2019/9/26
 */
@Retention(value = java.lang.annotation.RetentionPolicy.RUNTIME)
@Target(value = { java.lang.annotation.ElementType.TYPE })
@Documented
@Configuration
@Import({ CompositeSecurityConfigurer.class, AutowiredBeanConfigurer.class })
@EnableConfigurationProperties(SecurityProperties.class)
public @interface EnableLazySecurity {
}
