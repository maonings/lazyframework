package com.lazyframework.security.admin.authentication;

import io.jsonwebtoken.JwtException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Create by lazy in 2019/9/26
 */
public interface JwtAuthenticationFailureHandler {

    /**
     * Called when an authentication attempt fails.
     * @param request the request during which the authentication attempt occurred.
     * @param response the support.
     * @param exception the exception which was thrown to reject the jwt authentication
     * request.
     */
    void onAuthenticationFailure(HttpServletRequest request,
                                 HttpServletResponse response, JwtException exception)
            throws IOException, ServletException;

}
