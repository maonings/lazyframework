package com.lazyframework.security.admin.authentication;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Create by lazy in 2019/9/28
 */
public interface LoginLogService {

    void save(HttpServletRequest request, HttpServletResponse response, Authentication authentication);

}
