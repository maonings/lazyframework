package com.lazyframework.security.admin.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;

/**
 * Create by lazy in 2019.09.17
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "lazier.security")
public class SecurityProperties {

    private String[] permitAll = { "/**" };

    private String[] authenticated = {};

    private String logoutUrl = "/logout";

    private String loginProcessUrl = "/login";

    /**
     * 参数名
     */
    private String authenticationHeader = "Authorization";

    /**
     * JWT认证头：
     * Basic、Bearer、Digest、HOBA、Mutual、AWS4-HMAC-SHA25
     */
    private String authenticationType = "Bearer";

    /**
     * JWT Configuration
     */
    @NestedConfigurationProperty
    private JwtProperties jwt = new JwtProperties();

}
