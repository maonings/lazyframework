package com.lazyframework.security.web;

import com.lazyframework.security.web.authentication.token.Authentication;

/**
 * Create by lazy in 2019/11/6
 */
public interface SecurityContext {

    Authentication getAuthentication();

    void setAuthentication(Authentication authentication);

}
