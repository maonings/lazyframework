package com.lazyframework.security.web.access;

import com.lazyframework.security.web.authentication.token.Authentication;

import javax.servlet.http.HttpServletRequest;

public interface AuthorizeService {

    boolean hasPermission(Authentication authentication, HttpServletRequest request);

}
