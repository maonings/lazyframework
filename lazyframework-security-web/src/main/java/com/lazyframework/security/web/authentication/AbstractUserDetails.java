package com.lazyframework.security.web.authentication;

import java.util.Collection;
import java.util.Collections;

public abstract class AbstractUserDetails implements UserDetails {

    @Override
    public Collection<String> getAuthorities() {
        return Collections.emptyList();
    }

}
