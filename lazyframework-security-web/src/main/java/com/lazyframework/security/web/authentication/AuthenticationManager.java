package com.lazyframework.security.web.authentication;

import com.lazyframework.security.web.authentication.provider.AuthenticationProvider;
import com.lazyframework.security.web.authentication.token.Authentication;
import com.lazyframework.security.web.exception.AuthenticationException;

/**
 * Create by lazy in 2019/11/7
 */
public interface AuthenticationManager {

    Authentication authenticate(Authentication authentication) throws AuthenticationException;

    void registerProvider(AuthenticationProvider provider);

}
