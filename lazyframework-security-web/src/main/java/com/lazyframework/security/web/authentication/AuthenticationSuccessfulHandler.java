package com.lazyframework.security.web.authentication;

import com.lazyframework.security.web.authentication.token.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Create by lazy in 2019/11/7
 */
public interface AuthenticationSuccessfulHandler {

    void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException;

}
