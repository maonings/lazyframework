package com.lazyframework.security.web.authentication;

import com.lazyframework.security.web.exception.AuthenticationException;

import java.io.Serializable;

/**
 * Create by lazy in 2019/11/7
 */
public interface UserDetailsService {

    UserDetails loadById(Serializable id) throws AuthenticationException;

    default UserDetails loadByOpenID(String openID) throws AuthenticationException {
        return null;
    }

    default UserDetails loadByUsername(String username) throws AuthenticationException {
        return null;
    }

}
