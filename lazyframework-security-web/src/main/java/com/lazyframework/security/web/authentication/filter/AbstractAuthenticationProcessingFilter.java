package com.lazyframework.security.web.authentication.filter;

import com.lazyframework.security.web.authentication.AuthenticationManager;
import com.lazyframework.security.web.authentication.token.Authentication;
import com.lazyframework.security.web.exception.AuthenticationException;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Create by lazy in 2019/11/7
 */
public abstract class AbstractAuthenticationProcessingFilter extends GenericFilterBean {

    private AuthenticationManager authenticationManager;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (!requires(request)) {
            chain.doFilter(request, response);
            return;
        }

        // 认证
        Authentication authentication;

        try {
            authentication = attemptAuthentication(request, response);
            if (authentication == null) {
                return;
            }
        } catch (AuthenticationException ae) {
            unsuccessfulAuthentication(response, ae);
            return;
        }

        successfulAuthentication(request, response, authentication);

        if (response.isCommitted()) {
            return;
        }

        chain.doFilter(request, response);
    }

    public AuthenticationManager authenticationManager() {
        return this.authenticationManager;
    }

    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    protected abstract boolean requires(HttpServletRequest request);

    protected abstract Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException, IOException;

    protected abstract void unsuccessfulAuthentication(HttpServletResponse response, AuthenticationException ae) throws IOException;

    protected abstract void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException;
}
