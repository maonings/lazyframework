package com.lazyframework.security.web.authentication.filter;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lazyframework.commons.support.BasicResponseStatus;
import com.lazyframework.commons.support.ResponseModels;
import com.lazyframework.security.web.JwtClients;
import com.lazyframework.security.web.authentication.SecurityResponseStatus;
import com.lazyframework.security.web.authentication.UserDetails;
import com.lazyframework.security.web.authentication.token.Authentication;
import com.lazyframework.security.web.authentication.token.UsernamePasswordAuthenticationToken;
import com.lazyframework.security.web.exception.AuthenticationException;
import com.lazyframework.security.web.exception.BadCredentialsException;
import com.lazyframework.security.web.exception.UserNotFoundException;
import com.lazyframework.security.web.util.AntPathRequestMatcher;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Create by lazy in 2019/11/6
 */
@Component
public class UsernamePasswordAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private JwtClients jwtClients;

    private static final String DEFAULT_USERNAME_PARAMETER = "username";

    private static final String DEFAULT_PASSWORD_PARAMETER = "password";

    public void setJwtClients(JwtClients jwtClients) {
        this.jwtClients = jwtClients;
    }

    @Override
    protected boolean requires(HttpServletRequest request) {
        String loginProcessUrl = "/auth/pwd";
        return new AntPathRequestMatcher(loginProcessUrl, HttpMethod.POST.name()).matches(request);
    }

    @Override
    protected Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException, IOException {
        // Read parameters from JSON
        Map<String, String> parameters = new ObjectMapper().readValue(request.getInputStream(),
                new TypeReference<Map<String, String>>() {});

        String username = parameters.get(DEFAULT_USERNAME_PARAMETER);
        String password = parameters.get(DEFAULT_PASSWORD_PARAMETER);

        if (username == null) {
            username = "";
        }
        if (password == null) {
            password = "";
        }

        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);

        return authenticationManager().authenticate(token);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletResponse response, AuthenticationException ae) throws IOException {
        if (ae instanceof UserNotFoundException) {
            ResponseModels.print(response, SecurityResponseStatus.USERNAME_NOTFOUND);
        } else if (ae instanceof BadCredentialsException) {
            ResponseModels.print(response, SecurityResponseStatus.BAD_CREDENTIALS);
        } else {
            ResponseModels.print(response, BasicResponseStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException {
        UserDetails userDetails = (UserDetails) authentication.principal();
        // 生成JWT
        String token = jwtClients.encode(userDetails.getId());
        // 返回JWT
        ResponseModels.print(response, BasicResponseStatus.OK, token);
    }

}
