package com.lazyframework.security.web.authentication.provider;

import com.lazyframework.security.web.authentication.token.Authentication;
import com.lazyframework.security.web.exception.AuthenticationException;

/**
 * Create by lazy in 2019/11/7
 */
public interface AuthenticationProvider{

    Authentication authenticate(Authentication authentication) throws AuthenticationException;

    boolean support(Class<?> auth);

}
