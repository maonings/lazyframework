package com.lazyframework.security.web.authentication.provider;

import com.lazyframework.security.web.authentication.UserDetails;
import com.lazyframework.security.web.authentication.token.Authentication;
import com.lazyframework.security.web.authentication.token.UsernamePasswordAuthenticationToken;
import com.lazyframework.security.web.exception.AuthenticationException;
import com.lazyframework.security.web.exception.BadCredentialsException;
import com.lazyframework.security.web.exception.UserNotFoundException;
import com.lazyframework.security.web.util.PasswordEncoder;

/**
 * Create by lazy in 2019/11/7
 */
public class DaoAuthenticationProvider extends AbstractAuthenticationProvider {

    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication doAuthenticate(Authentication authentication) throws AuthenticationException {

        UsernamePasswordAuthenticationToken authenticationToken = (UsernamePasswordAuthenticationToken) authentication;

        String username = (String) authentication.principal();
        UserDetails userDetails = userDetailsService().loadByUsername(username);

        if (userDetails == null) {
            throw new UserNotFoundException();
        }

        // Match password
        if (!passwordEncoder.matches(authenticationToken.getPassword(), userDetails.getPassword())) {
            throw new BadCredentialsException();
        }

        return new UsernamePasswordAuthenticationToken(userDetails);
    }

    @Override
    public boolean support(Class<?> auth) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(auth);
    }

    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

}
