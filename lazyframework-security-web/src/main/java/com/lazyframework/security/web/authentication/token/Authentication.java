package com.lazyframework.security.web.authentication.token;

/**
 * Create by lazy in 2019/11/6
 */
public interface Authentication {

    Object principal();

    boolean isAuthenticated();

}
