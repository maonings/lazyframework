package com.lazyframework.security.web.authentication.token;

/**
 * Create by lazy in 2019/11/7
 */
public class JwtAuthenticationToken implements Authentication {

    private boolean isAuthenticated;

    private final Object principal;

    public JwtAuthenticationToken(Object principal) {
        this.principal = principal;
    }

    @Override
    public Object principal() {
        return this.principal;
    }

    @Override
    public boolean isAuthenticated() {
        return isAuthenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.isAuthenticated = authenticated;
    }
}
