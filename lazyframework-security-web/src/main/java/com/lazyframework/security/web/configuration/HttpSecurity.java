package com.lazyframework.security.web.configuration;

import com.lazyframework.security.web.SecurityFilterChain;
import com.lazyframework.security.web.authentication.AuthenticationManager;
import com.lazyframework.security.web.authentication.AuthenticationManagerImpl;
import com.lazyframework.security.web.authentication.filter.SecurityContextCacheClearFilter;
import com.lazyframework.security.web.configuration.configurer.*;
import com.lazyframework.security.web.extra.weixin.OpenIDAuthenticationConfigurer;

import javax.servlet.Filter;
import java.util.ArrayList;
import java.util.List;

/**
 * Create by lazy in 2019/11/12
 */
public class HttpSecurity extends AbstractConfiguredSecurityBuilder<Filter, HttpSecurity>
        implements SecurityBuilder<Filter> {

    private List<Filter> filters;

    public HttpSecurity() {
        this.filters = new ArrayList<>();
        this.filters.add(new SecurityContextCacheClearFilter());
    }

    public void addFilter(Filter filter) {
        filters.add(filter);
    }

    public AuthenticationManager authenticationManager() {
        return new AuthenticationManagerImpl();
    }

    public FilterSecurityInterceptorConfigurer authorizeRequests() {
        FilterSecurityInterceptorConfigurer configurer = new FilterSecurityInterceptorConfigurer();
        apply(configurer);
        return configurer;
    }

    public FormLoginConfigurer formLogin() {
        FormLoginConfigurer formLoginConfigurer = new FormLoginConfigurer();
        apply(formLoginConfigurer);
        return formLoginConfigurer;
    }

    public OpenIDAuthenticationConfigurer openid() {
        OpenIDAuthenticationConfigurer configurer = new OpenIDAuthenticationConfigurer();
        apply(configurer);
        return configurer;
    }

    public CorsConfigurer cors() {
        CorsConfigurer configurer = new CorsConfigurer();
        apply(configurer);
        return configurer;
    }

    public JwtAuthenticationConfigurer jwt() {
        JwtAuthenticationConfigurer configurer = new JwtAuthenticationConfigurer();
        apply(configurer);
        return configurer;
    }

    @Override
    protected Filter doBuild() {
        return new SecurityFilterChain(filters);
    }

}
