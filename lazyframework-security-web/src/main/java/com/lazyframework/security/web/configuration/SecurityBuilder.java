package com.lazyframework.security.web.configuration;

/**
 * 安全配置构建器接口模型
 * @param <O>
 */
public interface SecurityBuilder<O> {

    O build();

}
