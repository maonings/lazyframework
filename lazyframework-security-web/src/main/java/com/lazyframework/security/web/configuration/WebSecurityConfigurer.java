package com.lazyframework.security.web.configuration;

import javax.servlet.Filter;

public interface WebSecurityConfigurer<B extends SecurityBuilder<Filter>> extends SecurityConfigurer<Filter, B> {
}
