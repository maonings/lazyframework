package com.lazyframework.security.web.configuration.configurer;

import com.lazyframework.security.web.authentication.filter.CorsFilter;
import com.lazyframework.security.web.configuration.HttpSecurity;
import com.lazyframework.security.web.configuration.SecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfigurationSource;

import javax.servlet.Filter;

public class CorsConfigurer extends SecurityConfigurerAdapter<Filter, HttpSecurity> {

    private CorsConfigurationSource corsConfigurationSource;

    public CorsConfigurer corsConfigurationSource(CorsConfigurationSource corsConfigurationSource) {
        this.corsConfigurationSource = corsConfigurationSource;
        return this;
    }

    @Override
    public void configure(HttpSecurity builder) {
        builder.addFilter(new CorsFilter(corsConfigurationSource));
    }
}
