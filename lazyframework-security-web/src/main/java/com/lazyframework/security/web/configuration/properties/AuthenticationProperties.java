package com.lazyframework.security.web.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "lazier.security.authentication")
public class AuthenticationProperties {

}
