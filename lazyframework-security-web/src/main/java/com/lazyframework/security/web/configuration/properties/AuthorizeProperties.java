package com.lazyframework.security.web.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;
import java.util.Map;

@ConfigurationProperties(prefix = "lazier.security.authorize")
public class AuthorizeProperties {

    private String[] authenticated;

    private Map<String, List<String>> permissions;

    public void setAuthenticated(String[] authenticated) {
        this.authenticated = authenticated;
    }

    public void setPermissions(Map<String, List<String>> permissions) {
        this.permissions = permissions;
    }

    public String[] authenticated() {
        return authenticated;
    }

    public Map<String, List<String>> permissions() {
        return permissions;
    }

}
