package com.lazyframework.security.web.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.UUID;

@ConfigurationProperties(prefix = "lazier.security.jwt")
public class JsonWebTokenProperties {

    /**
     * 有效时间，单位：秒(s)
     */
    private int expire = 7200;

    /**
     * JWT签发者
     */
    private String issuer = "0x4D6E";

    /**
     * 如果不配置JWT秘钥，系统将使用随机UUID作为秘钥，每次服务重启都会重新生成新的秘钥
     */
    private String secretKey = UUID.randomUUID().toString();

    public void setExpire(int expire) {
        this.expire = expire;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public int expire() {
        return expire;
    }

    public String issuer() {
        return issuer;
    }

    public String secretKey() {
        return secretKey;
    }
}
