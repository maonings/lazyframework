package com.lazyframework.security.web.exception;

import io.jsonwebtoken.JwtException;

/**
 * Create by lazy in 2019/11/7
 */
public class JwtAuthenticationException extends AuthenticationException {

    private JwtException exception;

    public JwtAuthenticationException(JwtException e) {
        this.exception = e;
    }

    public JwtException getException() {
        return exception;
    }
}
