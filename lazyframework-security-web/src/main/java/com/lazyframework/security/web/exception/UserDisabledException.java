package com.lazyframework.security.web.exception;

/**
 * 用户被禁用异常
 *
 * @author maoning
 */
public class UserDisabledException extends AuthenticationException {

}
