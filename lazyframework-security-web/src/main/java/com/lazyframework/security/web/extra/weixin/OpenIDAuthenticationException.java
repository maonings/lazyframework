package com.lazyframework.security.web.extra.weixin;

import com.lazyframework.security.web.exception.AuthenticationException;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 微信小程序
 *
 * @author maoning
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class OpenIDAuthenticationException extends AuthenticationException {

    private String errcode;

    private String errmsg;

    public OpenIDAuthenticationException() {
    }

    public OpenIDAuthenticationException(String errcode, String errmsg) {
        this.errcode = errcode;
        this.errmsg = errmsg;
    }
}
