package com.lazyframework.security.web.extra.weixin;

import com.lazyframework.security.web.authentication.token.Authentication;
import com.lazyframework.security.web.extra.weixin.WxAuthService.WxAuthType;

/**
 * 写点注释吧，求你了!!!/\^_^/\
 *
 * @author maoning
 */
public class OpenIDAuthenticationToken implements Authentication {

    private final Object principal;

    private boolean isAuthenticated;

    private final WxAuthService.WxAuthType authType;

    public OpenIDAuthenticationToken(Object principal) {
        this.principal = principal;
        this.authType = WxAuthType.NONE;
    }

    public OpenIDAuthenticationToken(WxAuthType type, Object principal) {
        this.authType = type;
        this.principal = principal;
    }

    @Override
    public Object principal() {
        return this.principal;
    }

    @Override
    public boolean isAuthenticated() {
        return this.isAuthenticated;
    }

    public WxAuthType authType() {
        return authType;
    }

    public void setAuthenticated(boolean authenticated) {
        this.isAuthenticated = authenticated;
    }
}
