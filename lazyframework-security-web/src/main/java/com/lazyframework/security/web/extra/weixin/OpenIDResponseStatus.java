package com.lazyframework.security.web.extra.weixin;

import com.lazyframework.commons.support.ResponseStatus;

public enum OpenIDResponseStatus implements ResponseStatus {

    INVALID_CODE(1030, "无效的微信授权Code"),

    UNKNOWN_WX_AUTH_ERROR(1031, "未知的微信授权错误");

    private final int value;

    private final String reasonPhrase;

    OpenIDResponseStatus(int value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    @Override
    public int value() {
        return value;
    }

    @Override
    public String reasonPhrase() {
        return reasonPhrase;
    }
}
