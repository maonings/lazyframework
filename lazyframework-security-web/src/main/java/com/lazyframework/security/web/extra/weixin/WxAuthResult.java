package com.lazyframework.security.web.extra.weixin;

import lombok.Data;

/**
 * 写点注释吧，求你了!!!/\^_^/\
 *
 * @author maoning
 */
@Data
public class WxAuthResult {

    private String openid;

    /*==============================微信网页授权特有返回值=================================*/

    private String access_token;

    private String expires_in;

    private String refresh_token;

    private String scope;

    /*==============================小程序Code2Session特有返回值=================================*/

    private String unionid;

    private String session_key;

}
