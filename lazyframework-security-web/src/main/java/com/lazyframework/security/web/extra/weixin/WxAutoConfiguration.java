package com.lazyframework.security.web.extra.weixin;

import com.lazyframework.security.web.configuration.HttpSecurity;
import com.lazyframework.security.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty("lazier.security.weixin")
public class WxAutoConfiguration extends WebSecurityConfigurerAdapter {

    private String appid;

    private String secret;

    @Override
    protected void configure(HttpSecurity http) {
        http.openid().appid(appid).secret(secret)
                .jwtClients(jwtClients())
                .userDetailsService(userDetailsService());
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
