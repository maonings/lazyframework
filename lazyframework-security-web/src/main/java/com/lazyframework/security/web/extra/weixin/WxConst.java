package com.lazyframework.security.web.extra.weixin;

/**
 * 写点注释吧，求你了!!!/\^_^/\
 *
 * @author maoning
 */
public interface WxConst {

    /**
     * 小程序：通过code获取session_key和token
     */
    String URI_CODE2SESSION =
            "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code";

    /**
     * 微信网页授权：通过code换区access_token
     */
    String URI_CODE2ACCESS_TOKEN =
            "https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code";

}
